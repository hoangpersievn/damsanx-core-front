const { alias } = require("react-app-rewire-alias");
const { override, addLessLoader } = require("customize-cra");

const custom = require("./webpack.config.js");

const overrideAlias = (value) => (config) => {
  alias({
    // components: "src/components",
    // assets: "src/assets",
    // styles: "src/styles",
    // appRedux: "src/appRedux",
    // constants: "src/constants",
    // util: "src/util",
    // node_modules: "node_modules",
    ...custom.resolve.alias,
  })(config);
  return config;
};

module.exports = override(
  overrideAlias(),
  addLessLoader({
    // modifyVars: { "@primary-color": "#1DA57A" },
    lessOptions: {
      javascriptEnabled: true,
      modifyVars: { "@primary-color": "#1DA57A" },
    },
  })
);
