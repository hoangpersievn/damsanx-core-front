import React, { useMemo } from "react";
import PropTypes from "prop-types";
import {
  Area,
  AreaChart,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

const getPercent = (value, total) => {
  const ratio = total > 0 ? value / total : 0;

  return toPercent(ratio, 2);
};

const toPercent = (decimal, fixed = 0) => {
  return `${(decimal * 100).toFixed(fixed)}%`;
};
const renderTooltipContent = (o) => {
  const { payload, label } = o;
  const total = payload.reduce((result, entry) => result + entry.value, 0);

  return (
    <div className="customized-tooltip-content">
      <p className="total">{`${label} (Total: ${total})`}</p>
      <ul className="list">
        {payload.map((entry, index) => (
          <li key={`item-${index}`} style={{ color: entry.color }}>
            {`${entry.name}: ${entry.value}(${getPercent(entry.value, total)})`}
          </li>
        ))}
      </ul>
    </div>
  );
};

const PercentAreaChart = (props) => {
  const { data, areas } = props;

  const renderAreas = useMemo(
    () =>
      areas.map((item, index) => (
        <Area
          key={index}
          type={item.type}
          dataKey={item.dataKey}
          stackId={item.stackId}
          fillOpacity={item.fillOpacity}
          stroke={item.stroke}
          fill={item.fill}
        />
      )),
    [areas]
  );

  return (
    <ResponsiveContainer width="100%" height={200}>
      <AreaChart
        data={data}
        stackOffset="expand"
        margin={{ top: 10, right: 0, left: -15, bottom: 0 }}
      >
        <XAxis dataKey="month" />
        <YAxis tickFormatter={toPercent} />
        <CartesianGrid strokeDasharray="3 3" />
        <Tooltip content={renderTooltipContent} />
        {areas && areas.length > 0 && renderAreas}
      </AreaChart>
    </ResponsiveContainer>
  );
};

export default PercentAreaChart;

PercentAreaChart.propTypes = {
  data: PropTypes.arrayOf(PropTypes.any),
  areas: PropTypes.arrayOf(PropTypes.any),
};

PercentAreaChart.defaultProps = {
  data: [],
  areas: [],
};
