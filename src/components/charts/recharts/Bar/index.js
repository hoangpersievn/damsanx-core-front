import React, { useMemo } from "react";
import PropTypes from "prop-types";
import {
  Bar,
  BarChart,
  CartesianGrid,
  Legend,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

// import data from "./data";

const TinyBarChart = (props) => {
  const { data, bars } = props;

  const renderBars = useMemo(
    () =>
      bars.map((item, index) => (
        <Bar
          key={index}
          dataKey={item.dataKey}
          stackId={item.stackId}
          fill={item.fill}
        />
      )),
    [bars]
  );

  return (
    <ResponsiveContainer width="100%" height={200}>
      <BarChart
        data={data}
        margin={{ top: 10, right: 0, left: -15, bottom: 0 }}
      >
        <XAxis dataKey="name" />
        <YAxis />
        <CartesianGrid strokeDasharray="3 3" />
        <Tooltip />
        <Legend />
        {bars && bars.length > 0 && renderBars}
      </BarChart>
    </ResponsiveContainer>
  );
};

export default TinyBarChart;

TinyBarChart.propTypes = {
  data: PropTypes.arrayOf(PropTypes.any),
  bars: PropTypes.arrayOf(PropTypes.any),
};

TinyBarChart.defaultProps = {
  data: [],
  bars: [],
};
