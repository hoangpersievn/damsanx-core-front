import React, { useMemo } from "react";
import PropTypes from "prop-types";
import {
  Area,
  AreaChart,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

// import data from "./data";

const StackedAreaChart = (props) => {
  const { data, areas } = props;

  const renderAreas = useMemo(
    () =>
      areas.map((item, index) => (
        <Area
          key={index}
          type={item.type}
          dataKey={item.dataKey}
          stackId={item.stackId}
          fillOpacity={item.fillOpacity}
          stroke={item.stroke}
          fill={item.fill}
        />
      )),
    [areas]
  );

  return (
    <ResponsiveContainer width="100%" height={200}>
      <AreaChart
        data={data}
        margin={{ top: 10, right: 0, left: -15, bottom: 0 }}
      >
        <XAxis dataKey="name" />
        <YAxis />
        <CartesianGrid strokeDasharray="3 3" />
        <Tooltip />
        {areas && areas.length > 0 && renderAreas}
      </AreaChart>
    </ResponsiveContainer>
  );
};

export default StackedAreaChart;

StackedAreaChart.propTypes = {
  data: PropTypes.arrayOf(PropTypes.any),
  areas: PropTypes.arrayOf(PropTypes.any),
};

StackedAreaChart.defaultProps = {
  data: [],
  areas: []
};
