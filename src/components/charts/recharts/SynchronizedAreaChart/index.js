import React from "react";
import PropTypes from "prop-types";
import {
  Area,
  AreaChart,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

// import data from "./data";

const renderAreas = (areasArr) => {
  if (!areasArr || areasArr.length < 0) return null;

  return areasArr.map((item, index) => (
    <Area
      key={index}
      type={item.type}
      dataKey={item.dataKey}
      stackId={item.stackId}
      fillOpacity={item.fillOpacity}
      stroke={item.stroke}
      fill={item.fill}
    />
  ));
};

const SynchronizedAreaChart = (props) => {
  const { data, firstAreas, secondAreas, firstTitle, secondTitle } = props;

  return (
    <ResponsiveContainer width="100%">
      <div>
        <p className="mb-4">{firstTitle}</p>
        <div className="mb-4">
          <ResponsiveContainer width="100%" height={200}>
            <AreaChart
              data={data}
              syncId="anyId"
              margin={{ top: 10, right: 0, left: -15, bottom: 0 }}
            >
              <XAxis dataKey="name" />
              <YAxis />
              <CartesianGrid strokeDasharray="3 3" />
              <Tooltip />
              {renderAreas(firstAreas)}
            </AreaChart>
          </ResponsiveContainer>
        </div>
        <p className="mb-4">{secondTitle}</p>
        <div className="mb-4">
          <ResponsiveContainer width="100%" height={200}>
            <AreaChart
              data={data}
              syncId="anyId"
              margin={{ top: 10, right: 0, left: -15, bottom: 0 }}
            >
              <XAxis dataKey="name" />
              <YAxis />
              <CartesianGrid strokeDasharray="3 3" />
              <Tooltip />
              {renderAreas(secondAreas)}
            </AreaChart>
          </ResponsiveContainer>
        </div>
      </div>
    </ResponsiveContainer>
  );
};

export default SynchronizedAreaChart;

SynchronizedAreaChart.propTypes = {
  data: PropTypes.arrayOf(PropTypes.any),
  secondAreas: PropTypes.arrayOf(PropTypes.any),
  firstAreas: PropTypes.arrayOf(PropTypes.any),
  firstTitle: PropTypes.string,
  secondTitle: PropTypes.string,
};

SynchronizedAreaChart.defaultProps = {
  firstAreas: [],
  secondAreas: [],
  data: [],
  firstTitle: "",
  secondTitle: "",
};
