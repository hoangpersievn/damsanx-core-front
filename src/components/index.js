import AreaChart from "components/charts/recharts/Area";
import PercentAreaChart from "components/charts/recharts/PercentAreaChart";
import SynchronizedAreaChart from "components/charts/recharts/SynchronizedAreaChart";
import BarChart from "components/charts/recharts/Bar";

import Basic from "components/dataDisplay/avatar/Basic";
// import WithBadge from "components/dataDisplay/Avatar/WithBadge";
import SignIn from "components/userAuth/SignIn";

import "assets/vendors/style";

const avatar = {
  Basic,
};

const dataDisplay = {
  avatar,
};

const charts = {
  AreaChart,
  PercentAreaChart,
  SynchronizedAreaChart,
  BarChart,
};

export { charts, dataDisplay, SignIn };
