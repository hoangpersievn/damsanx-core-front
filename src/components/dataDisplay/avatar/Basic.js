import React from "react";
import PropTypes from "prop-types";

import { Avatar } from "antd";
import UserOutlined from "@ant-design/icons/lib/icons/UserOutlined";

const Basic = (props) => {
  const { size, icon, shape } = props;

  return (
    <Avatar
      className="gx-mr-2"
      shape={shape}
      size={size}
      icon={icon || <UserOutlined />}
    />
  );
};

export default Basic;

Basic.propTypes = {
  /**
   * The size of the avatar
   */
  size: PropTypes.oneOf(["large", "small", "default"]),
  /**
   * The shape of avatar
   */
  shape: PropTypes.oneOf(["circle", "square"]),
  /**
   * Custom icon type for an icon avatar
   */
  icon: PropTypes.element,
};

Basic.defaultProps = {
  size: "default",
  shape: "circle",
};
