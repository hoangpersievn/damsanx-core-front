import React from "react";

import { charts } from "components";
import { data } from "../data";

const { SynchronizedAreaChart } = charts;

export default {
  title: "Components/Charts/ReCharts/Areas/Synchrozied",
  component: SynchronizedAreaChart,
};

const Template = (args) => <SynchronizedAreaChart {...args} />;

export const SynchronizedArea = Template.bind({});
SynchronizedArea.args = {
  data,
  firstAreas: [
    {
      type: "monotone",
      dataKey: "uv",
      fillOpacity: 1,
      stroke: "#003366",
      fill: "#003366",
    },
  ],
  secondAreas: [
    {
      type: "monotone",
      dataKey: "price",
      fillOpacity: 1,
      stroke: "#FE9E15",
      fill: "#FE9E15",
    },
  ],
  firstTitle: "chart 1",
  secondTitle: "chart 2",
};
