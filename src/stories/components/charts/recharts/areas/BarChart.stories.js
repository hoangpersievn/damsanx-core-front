import React from "react";

import { charts } from "components";
import { data, areas, stackedAreas, connectNullsData, barAreas } from "../data";

const { BarChart } = charts;

export default {
  title: "Components/Charts/ReCharts/Bars",
  component: BarChart,
};

const Template = (args) => <BarChart {...args} />;

export const TinyBarChart = Template.bind({});
TinyBarChart.args = {
  data,
  bars: barAreas,
};

export const StackedBarChart = Template.bind({});
StackedBarChart.args = {
  data,
  bars: [
    {
      dataKey: "price",
      fill: "#003366",
      stackId: "a",
    },
    {
      dataKey: "uv",
      fill: "#FE9E15",
      stackId: "a",
    },
  ],
};

export const MixBarChart = Template.bind({});
MixBarChart.args = {
  data,
  bars: [
    {
      dataKey: "amt",
      fill: "#003366",
      stackId: "a",
    },
    {
      dataKey: "price",
      fill: "#FE9E15",
      stackId: "a",
    },
    {
      dataKey: "uv",
      fill: "#FE9E15",
    },
  ],
};
