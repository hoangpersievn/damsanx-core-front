import React from "react";

import { charts } from "components";
import { percentData, percentAreas } from "../data";

const { PercentAreaChart } = charts;

export default {
  title: "Components/Charts/ReCharts/Areas/Percent",
  component: PercentAreaChart,
};

const Template = (args) => <PercentAreaChart {...args} />;

export const PercentArea = Template.bind({});
PercentArea.args = {
  data: percentData,
  areas: percentAreas,
};
