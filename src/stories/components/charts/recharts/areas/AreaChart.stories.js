import React from "react";

import { charts } from "components";
import { data, areas, stackedAreas, connectNullsData } from "../data";

const { AreaChart } = charts;

export default {
  title: "Components/Charts/ReCharts/Areas",
  component: AreaChart,
};

const Template = (args) => <AreaChart {...args} />;

export const SimpleAreaChart = Template.bind({});
SimpleAreaChart.args = {
  data,
  areas,
};

export const AreaChartConnectNull = Template.bind({});
AreaChartConnectNull.args = {
  data: connectNullsData,
  areas,
};

export const StackedAreaChart = Template.bind({});
StackedAreaChart.args = {
  data,
  areas: stackedAreas,
};
