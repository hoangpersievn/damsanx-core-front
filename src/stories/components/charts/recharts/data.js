const data = [
  { name: "Page A", uv: 4000, price: 2400, amt: 2400 },
  { name: "Page B", uv: 3000, price: 1398, amt: 2210 },
  { name: "Page C", uv: 2000, price: 9800, amt: 2290 },
  { name: "Page D", uv: 2780, price: 3908, amt: 2000 },
  { name: "Page E", uv: 1890, price: 4800, amt: 2181 },
  { name: "Page F", uv: 2390, price: 3800, amt: 2500 },
  { name: "Page G", uv: 3490, price: 4300, amt: 2100 },
];

const percentData = [
  { name: "2015.01", a: 4000, b: 2400, c: 2400 },
  { name: "2015.02", a: 3000, b: 1398, c: 2210 },
  { name: "2015.03", a: 2000, b: 9800, c: 2290 },
  { name: "2015.04", a: 2780, b: 3908, c: 2000 },
  { name: "2015.05", a: 1890, b: 4800, c: 2181 },
  { name: "2015.06", a: 2390, b: 3800, c: 2500 },
  { name: "2015.07", a: 3490, b: 4300, c: 2100 },
];

const connectNullsData = [
  { name: "Page A", uv: 4000 },
  { name: "Page B", uv: 3000 },
  { name: "Page C", uv: 2000 },
  { name: "Page D" },
  { name: "Page E", uv: 1890 },
  { name: "Page F", uv: 2390 },
  { name: "Page G", uv: 3490 },
];

const areas = [
  {
    type: "monotone",
    dataKey: "uv",
    stackId: "1",
    fillOpacity: 1,
    stroke: "#003366",
    fill: "#003366",
  },
];

const stackedAreas = [
  {
    type: "monotone",
    dataKey: "uv",
    stackId: "1",
    fillOpacity: 1,
    stroke: "#003366",
    fill: "#003366",
  },
  {
    type: "monotone",
    dataKey: "price",
    stackId: "1",
    fillOpacity: 1,
    stroke: "#59AA2B",
    fill: "#59AA2B",
  },
  {
    type: "monotone",
    dataKey: "amt",
    stackId: "1",
    fillOpacity: 1,
    stroke: "#FE9E15",
    fill: "#FE9E15",
  },
];

const percentAreas = [
  {
    type: "monotone",
    dataKey: "a",
    stackId: "1",
    fillOpacity: 1,
    stroke: "#003366",
    fill: "#003366",
  },
  {
    type: "monotone",
    dataKey: "b",
    stackId: "1",
    fillOpacity: 1,
    stroke: "#59AA2B",
    fill: "#59AA2B",
  },
  {
    type: "monotone",
    dataKey: "c",
    stackId: "1",
    fillOpacity: 1,
    stroke: "#FE9E15",
    fill: "#FE9E15",
  },
];

const barAreas = [
  {
    dataKey: "price",
    fill: "#003366",
  },
  {
    dataKey: "uv",
    fill: "#FE9E15",
  },
];

export {
  data,
  percentData,
  connectNullsData,
  areas,
  stackedAreas,
  percentAreas,
  barAreas,
};
