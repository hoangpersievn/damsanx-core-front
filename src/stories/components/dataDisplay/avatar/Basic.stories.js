import React from "react";

import { dataDisplay } from "components";

const { Basic } = dataDisplay.avatar;

export default {
  title: "Components/Data Display/Avatar",
  component: Basic,
};

const Template = (args) => <Basic {...args} />;

export const Default = Template.bind({});
Default.args = {};

export const Large = Template.bind({});
Large.args = {
  size: "large",
  shape: "square",
};

export const Small = Template.bind({});
Small.args = {
  size: "small",
};
