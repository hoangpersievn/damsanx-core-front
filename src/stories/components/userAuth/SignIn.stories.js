import React from "react";

import { SignIn } from "components";

export default {
  title: "Components/Authentication/Sign In",
  component: SignIn,
};

const Template = (args) => <SignIn {...args} />;

export const Default = Template.bind({});
Default.args = {};
