import React from "react";
import { BrowserRouter as Router } from "react-router-dom";

import { dataDisplay, SignIn } from "components";

import "./App.scss";

const { Basic } = dataDisplay.avatar;

function App() {
  return (
    <Router>
      <div className="App">
        {/* <Basic size="large" /> */}
        <SignIn />
      </div>
    </Router>
  );
}

export default App;
