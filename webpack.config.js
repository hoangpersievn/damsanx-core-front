const path = require("path");

module.exports = {
  mode: "development",
  devtool: "inline-source-map",

  entry: "components/index.js",

  output: {
    path: path.resolve("dist"),
    filename: "index.js",
    libraryTarget: "commonjs2",
  },

  module: {
    rules: [
      {
        test: /\.js$/i,
        exclude: /node_modules/,
        loader: "babel-loader",
      },

      {
        test: /\.(css|s[ac]ss)$/i,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" },
          { loader: "sass-loader" },
        ],
      },

      {
        test: /\.less$/i,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" },
          {
            loader: "less-loader",
            options: {
              lessOptions: {
                javascriptEnabled: true,
                modifyVars: { "@primary-color": "#1DA57A" },
              },
            },
          },
        ],
      },

      {
        test: /\.(png|svg|jpg|gif)$/,
        loader: "url-loader",
        options: {
          name: "[path][name].[ext]",
        },
      },

      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      },

      // {
      //   test: /\.mdx$/,
      //   use: ["babel-loader", "@mdx-js/loader"],
      // },
    ],
  },

  resolve: {
    alias: {
      components: path.resolve(__dirname, "src/components/"),
      assets: path.resolve(__dirname, "src/assets/"),
      styles: path.resolve(__dirname, "src/styles"),
      appRedux: path.resolve(__dirname, "src/appRedux"),
      constants: path.resolve(__dirname, "src/constants"),
      util: path.resolve(__dirname, "src/util"),
      lngProvider: path.resolve(__dirname, "src/lngProvider"),
      node_modules: path.resolve(__dirname, "./node_modules"),
    },
  },

  externals: {
    react: "react",
  },
};
