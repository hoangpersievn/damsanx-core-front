const path = require("path");

module.exports = {
  stories: ["../src/**/*.stories.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx)"],

  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    {
      name: "@storybook/preset-create-react-app",
      options: {
        craOverrides: {
          fileLoaderExcludes: ["less"],
        },
      },
    },
  ],

  webpackFinal: async (config) => {
    config.module.rules.push({
      test: /\.less$/i,
      use: [
        { loader: "style-loader" },
        { loader: "css-loader" },
        {
          loader: "less-loader",
          options: {
            lessOptions: {
              javascriptEnabled: true,
              modifyVars: { "@primary-color": "#1DA57A" },
            },
          },
        },
      ],
    });

    return {
      ...config,

      resolve: {
        alias: {
          components: path.resolve(__dirname, "../src/components"),
          assets: path.resolve(__dirname, "../src/assets/"),
          styles: path.resolve(__dirname, "../src/styles"),
          // node_modules: path.resolve(__dirname, "../node_modules"),
          // appRedux: path.resolve(__dirname, "../src/appRedux"),
          // constants: path.resolve(__dirname, "../src/constants"),
          // util: path.resolve(__dirname, "../src/util"),
        },
      },
    };
  },
};
