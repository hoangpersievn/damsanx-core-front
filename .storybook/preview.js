import React from 'react';
import { addDecorator } from "@storybook/react";

import Wrapper from "components/Wrapper";

addDecorator((story) => <Wrapper>{story()}</Wrapper>);

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
};
